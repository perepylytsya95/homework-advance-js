// !Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
// !Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.


// !Не пишите, пожалуйста, громоздких решений, где вся логика находится в одной функции, в одном вложенном друг в друга потоке then-ов.
// *Подумайте над разделением ответственности, над функциями.
// ?Допустим, функция, которая получает данные с сервера и возвращает их. Функция, которая рендерит фильмы. Функция, которая рендерит персонажей.
// Старайтесь нигде не перечислять руками никаких свойств объектов вручную. Используйте Object.keys / Object.values / Object.entries, метод map и т.д., Element.insertAdjacentHTML() для вставки на страницу. Если нужно ограниченное количество свойств - используйте отдельный массив. Не пишите бесконечные ряды однотипных строк кода типа создали элемент createElement , потом записали класс, потом еще какие-то атрибуты, потом заапендили на страницу. Это невозможно ни читать, ни сопровождать потом. ///*Для этого тоже можно создать функцию createDOMElement, которая принимает необходимые аргументы, создает элемент и возвращает.
//*Для вложенных запросов (по персонажам) используйте Promise.all
// Можете использовать даже классы (ООП), если есть идеи по этой части как организовать классы и методы.
// Помним, что это продвинутый JS. Просто "работает" не принимается. Код красивый, масштабируемый, читаемый, поддерживаемый. Написали первую версию - не спешим отправлять. Думаем над оптимизацией, где что можно улучшить, где есть смысл выводить в отдельную функцию, где нет смысла и так далее


const display = () => {
	renderFilms()
	renderCharacters()
}

const getData = (url) => {
	return fetch(url)
		.then(response => response.json())
}

const renderFilms = () => {
	const ul = document.createElement('ul')
	document.body.append(ul)

	getData('https://ajax.test-danit.com/api/swapi/films')
		.then(films => {
			films.forEach(film => {
				const filmElement =
				`<li>
				<span style='color:red'>Episode: ${film.episodeId}</span>, ${film.name}, Opening Crawl: ${film.openingCrawl}
				</li>
				<li id='characters${film.id}' style='list-style:none'>
				<div class="loader" data-animation=${film.id}></div>
				</li>`
				ul.insertAdjacentHTML('beforeend', filmElement)
			})
		})
}

const renderCharacters = () => {
	getData('https://ajax.test-danit.com/api/swapi/films')
		.then(films => {
			films.forEach(film => {
				const charactersElement = document.querySelector(`#characters${film.id}`)
				const animationElement = document.querySelector(`[data-animation='${film.id}']`)
				const innerOl = document.createElement('ol')
				charactersElement.append(innerOl)

				const charactersUrls = film.characters
				const requestCharacters = charactersUrls.map(url => fetch(url))
				Promise.all(requestCharacters)
					.then(responses => Promise.all(responses.map(response => response.json())))
					.then(characters => {
						characters.forEach(character => {
							const characterInfo = 
							`<li>
							${character.name},
							gender: ${character.gender ? character.gender : 'no gender'},
							birth year: ${character.birthYear ? character.birthYear : 'unknown'},
							height: ${character.height ? character.height : 'height unknown'},
							mass: ${character.mass ? character.mass : 'mass unknown'},
							eye color: ${character.eyeColor ? character.eyeColor : 'no eyes'},
							skin color: ${character.skinColor ? character.skinColor : 'no skin'},
							hair color: ${character.hairColor ? character.hairColor : 'no hair'}
							</li>`
							animationElement.remove()
							innerOl.insertAdjacentHTML('afterbegin', characterInfo)
						})
					})
			})
		})
}

display()