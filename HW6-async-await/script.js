// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const MY_KEY = '4cb1a11bdf5f834fba4f7f9b042d6220'
const ipInfoBlock = document.querySelector('.ip__info')
const searchBtn = document.querySelector('.ip__btn')
const requirements = ['continent_name', 'country_name', 'region_name', 'city', 'zip', 'ip']

const getIp = async () => {
	const response = await fetch('https://api.ipify.org/?format=json')
	const ipData = await response.json()
	return ipData.ip
}

const getInfo = async (ip) => {
	const response = await fetch(`http://api.ipstack.com/${ip}?access_key=${MY_KEY}`)
	const info = await response.json()
	return info
}

searchBtn.addEventListener('click', async () => {
	searchBtn.disabled = true
	const info = await getInfo(await getIp())
	console.log(info)
	const infoElements = requirements.map((requirement) => `<p style='margin: 10px; font-size: 16px'>${requirement}: ${info[requirement]}</p>`)
	console.log(infoElements)
	ipInfoBlock.innerText = ""
	ipInfoBlock.innerHTML = infoElements.join('')
})