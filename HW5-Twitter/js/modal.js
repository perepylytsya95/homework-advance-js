import Card from "./card.js"

// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.

class Modal extends Card {
	constructor() {
		super()
	}

	renderModal() {
		const modalWrapper = this.createElement('div', ['modal-wrapper'])
		modalWrapper.innerHTML = `
		<button type="button" class="btn btn-outline-light" data-bs-toggle="modal" data-bs-target="#exampleModal">
			Add new post
		</button>
	
		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">New post</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">

						<label for="post-title">Type in new post title</label>
						<input type="text" id="post-title"></input>
						<label for="post-text">Type in new post text</label>
						<textarea style="height: 300px" id="post-text"></textarea>

					</div>
					<div class="modal-footer">
						<button id="submit-post" type="button" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
					</div>
				</div>
			</div>
		</div>`
		this.root.insertAdjacentElement('beforebegin', modalWrapper)
	}

	addNewPost() {
		const postTitle = document.querySelector('#post-title')
		const postText = document.querySelector('#post-text')
		const submitButton = document.querySelector('#submit-post')

		submitButton.addEventListener('click', () => {
			fetch('https://ajax.test-danit.com/api/json/posts', {
				method: 'POST',
				body: JSON.stringify({
					userId: 1,
					title: postTitle.value,
					body: postText.value
				})
			})
				.then(response => response.json())
				.then(post => {
					console.log(post)
					this.newCard =
					`<div class='card' id=${post.id}>
						<div class='userInfo'>
							<span class='userInfo__name'>Leanne Graham</span>
							<span class='userInfo__email'><a href='Sincere@april.biz'>Sincere@april.biz</a></span>
						</div>
						<div class='userPost'>
							<p class='userPost__title'>${post.title}</p>
							<p class='userPost__body'>${post.body}</p>
						</div> 
						<svg data-id=${post.id} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
						<path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
						<path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
						</svg>
					</div>`
					this.root.insertAdjacentHTML('afterBegin', this.newCard)
				})
		})
	}
}

export default Modal