class Card {
	constructor() {
		this.root = document.querySelector('#root')
		this.deleteCards()
	}

	createElement(elemType, classNames, text) {
		const element = document.createElement(elemType);
		if (text) { element.textContent = text; }
		element.classList.add(...classNames);
		return element
	}

	renderCards() {
		fetch(`https://ajax.test-danit.com/api/json/users`)
			.then(response => response.json())
			.then(allUsers => {

				allUsers.forEach(user => {
					fetch(`https://ajax.test-danit.com/api/json/posts?userId=${user.id}`)
						.then(response => response.json())
						.then(posts => {
							posts.forEach(post => {
								this.userElement =
									`<div class='card' id=${post.id}>
									<div class='userInfo'>
										<span class='userInfo__name'>${user.name}</span>
										<span class='userInfo__email'><a href='${user.email}'>${user.email}</a></span>
									</div>
									<div class='userPost'>
										<p class='userPost__title'>${post.title}</p>
										<p class='userPost__body'>${post.body}</p>
									</div> 
									<svg data-id=${post.id} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
									<path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
									<path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
								</svg>
								</div>`
								this.root.insertAdjacentHTML('afterBegin', this.userElement)
							})
						})
				});
			})
	}

	deleteCards() {
		this.root.addEventListener('click', (event) => {
			const deleteIcon = event.target.closest('[data-id]')

			if (deleteIcon) {
				const targetDeleteId = deleteIcon.getAttribute('data-id')
				const targetDeleteCard = document.getElementById(targetDeleteId)
				
				fetch(`https://ajax.test-danit.com/api/json/posts/${targetDeleteId}`, {
					method: 'DELETE'
				})
					.then(response => {
						targetDeleteCard.remove()
					})
			}
		})
	}
}

export default Card