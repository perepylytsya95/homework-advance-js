// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

// Сделайте его масштабируемым.
// Представьте, что в объекте не 3 свойства надо проверять, а 100.
// Подойдет ли решение с if - else if - else if - ............. - else?
// Или может лучше объявить массив обязательных названий свойств и проверять наличие этих свойств сквозь итерацию массива?
// Для записи данных в DOM-дерево может было бы интересно использовать Object.entries + arr.map + шаблонную строку? Сформировать HTML-строку и потом с помощью Element.insertAdjacentHTML() внедрить в DOM-дерево... Или другие идеи.
// В общем, это продвинутый JS. Задача не просто сделать, чтобы это работало, а сделать красиво, показать уровень. Спешить некуда. Но надо успеть в рамках дэдлайнов.

const books = [
	{
		author: "Скотт Бэккер",
		name: "Тьма, что приходит прежде",
		price: 70
	},
	{
		author: "Скотт Бэккер",
		name: "Воин-пророк",
	},
	{
		name: "Тысячекратная мысль",
		price: 70
	},
	{
		author: "Скотт Бэккер",
		name: "Нечестивый Консульт",
		price: 70
	},
	{
		author: "Дарья Донцова",
		name: "Детектив на диете",
		price: 40
	},
	{
		author: "Дарья Донцова",
		name: "Дед Снегур и Морозочка",
	}
];

const requirements = ['author', 'name', 'price']
const root = document.querySelector('#root')

function displayBooks(books, requirements) {
	const booksToDisplay = books.filter((book) => {
		try {
			for (let i = 0; i < requirements.length; i++) {
				if (!(requirements[i] in book)) {
					throw new Error(`${book.name} doesn't have ${requirements[i]}`)
				}
			}
			
			return book
		} catch (error) {
			console.error(error.message)
		}
	})

	const elements = booksToDisplay.map(book => `<li>${book.author}, ${book.name}, ${book.price}</li>`)

	root.insertAdjacentHTML('afterbegin', `<ul>${elements.join(' ')}</ul>`)
}

displayBooks(books, requirements)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// const ul = document.createElement('ul')
// const root = document.querySelector('#root').append(ul)

// books.forEach((object) => {
// 	if (Object.keys(object).includes('author') && Object.keys(object).includes('name') && Object.keys(object).includes('price')) {
// 		const li = document.createElement('li')
// 		li.innerText = `${object.author}, ${object.name}, ${object.price}`
// 		ul.append(li)
// 	}
// 	try {
// 		if (!Object.keys(object).includes('author')) {
// 			console.log(object)
// 			throw new Error(`This object doesn't have AUTHOR`)
// 		}
// 		if (!Object.keys(object).includes('name')) {
// 			console.log(object)
// 			throw new Error(`This object doesn't have NAME`)
// 		}
// 		if (!Object.keys(object).includes('price')) {
// 			console.log(object)
// 			throw new Error(`This object doesn't have PRICE`)
// 		}
// 	} catch (error) {
// 		console.error(error.message)
// 	}
// })