// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
// Создайте геттеры и сеттеры для этих свойств.
// Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров объекта Programmer, выведите их в консоль.


class Employee {
	constructor(name, age, salary) {
		this._name = name
		this._age = age
		this._salary = salary
	}
	get name() {
		return this._name
	}
	set name(value) {
		this._name = value
	}
	get age() {
		return this._age
	}
	set age(value) {
		this._age = value
	}
	get salary() {
		return this._salary
	}
	set salary(value) {
		this._salary = value
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary)
		this.lang = lang
	}
	get salary() {
		return this._salary * 3
	}
	set salary(value) {
		return this._salary = value
	}
}

const employee1 = new Employee('Lisa', 22, 22000)

const programmerBob = new Programmer('Bob', 33, 32000, ['english', 'chinese', 'russian', 'ukrainian'])
const programmerAlice = new Programmer('Alice', 59, 1000, ['french', 'polish', 'vietnamese'])
const programmerWilliam = new Programmer('William', 31, 90000, ['german', 'slovakian', 'spanish'])
console.log(programmerBob)
console.log(programmerAlice)
console.log(programmerWilliam)